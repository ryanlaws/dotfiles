" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set nobackup		" do not keep a backup file, use versions instead
set nowritebackup
set noswapfile     "no swap files
set history=100		" keep 50 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set incsearch		" do incremental searching
set noautoindent    " fuck auto-indent
set formatoptions-=cro " no auto-comment

" word wrap stuff
set wrap
set linebreak
set nolist  " list disables linebreak
set textwidth=0
set wrapmargin=0

" Don't use Ex mode, use Q for formatting
map Q gq

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
	" set mouse=a
endif

" mouse scrolling for urxvt
set mouse=a

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
  set incsearch
endif

" edits
set wrap
set wrapscan
set number
set relativenumber

" colours
colorscheme uoou

" use normal cliphoard not crazy primary bullshit
set clipboard=unnamedplus

" size of a hard tabstop
set tabstop=4

" " size of an "indent"
set shiftwidth=4

" buffers hidden by default
set hidden

" highlight current line
set cursorline

" cursor shapes
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" case insensitive (and smart) search
set ignorecase
set smartcase

" cooler completion
set wildmenu

" redraw less
set lazyredraw

" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" move vertically by visual line
nnoremap j gj
nnoremap k gk
set spell spelllang=en_gb
set autoindent		" always set autoindenting on
set spellsuggest=best,10	" z= to bring up corrections
set nospell

" remember buffers
set viminfo^=%

" Spellchecking
nn <F7> :setlocal spell! spell?<CR>

" Save as root
cmap w!! w !sudo tee > /dev/null %

" fuzzy find files n ting
syntax enable
filetype plugin on
" search sub-dirs
set path+=**
" tab complete
set wildmenu

" RG footnotes
:command RG1 %s/\v^\[\^(\d+)\]:/\<a name="f\1"\>\<\/a\>[\1](#fn\1)./g
:command RG2 %s/\v\[\^(\d+)\]/\<a name="fn\1"\>\<\/a\>\<span class="footnote"\>[\1](#f\1)\<\/span\>/g
