# highlighting and suggestions
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# key bindings
bindkey -e # emacs style keys
bindkey '^[[3~' delete-char # delete
bindkey '^P' up-line-or-search # ctrl-p like up arrow
bindkey -r '^G' # annoying

# newline on prompt & git branch indicator
function precmd {
    if [[ "$NEW_LINE" = true ]] then
		print ""
    else
        NEW_LINE=true
    fi
	vcs_info
#	emoji=$(cat .local/bin/emoji_list_single | shuf | head -n1 | sed "s/ .*//")
}
autoload -Uz vcs_info
alias clear='NEW_LINE=false && clear' # no preceeding newline after clear
zstyle ':vcs_info:git:*' formats '%b '

# Set up the prompt (with git branch name)
setopt PROMPT_SUBST
#PROMPT='${emoji} %B%F{cyan}${vcs_info_msg_0_}%F{red}%1~%F{white} %# %f%b'
PROMPT='%B%F{cyan}${vcs_info_msg_0_}%F{red}%1~%F{white} %# %f%b'

# history
HISTSIZE=10000
if (( ! EUID )); then
	HISTFILE=~/.history_root
else
	HISTFILE=~/.history
fi
SAVEHIST=5000
setopt HIST_IGNORE_SPACE
setopt SHARE_HISTORY
setopt HIST_IGNORE_ALL_DUPS

# fix some keys (del and stuff)
function zle-line-init () { echoti smkx }
function zle-line-finish () { echoti rmkx }
zle -N zle-line-init
zle -N zle-line-finish

# ls colours
test -r "$HOME/.dircolours" && eval $(dircolors ~/.dircolours)

# general options
setopt auto_cd
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # case-insensitive matching
setopt completealiases
plugins=(zsh-completions)
autoload -Uz compinit
compinit

# aliases
alias cp='cp -v'
alias mv='mv -v'
alias cal='cal -m'
alias df='df -h'
alias ls='ls --classify --tabsize=0 --group-directories-first --literal --color=auto --show-control-chars --human-readable'
alias ll='ls -l'
alias la='ls -lA'
alias ld='ls -d'
alias tree='tree -C'
alias which='where'
alias ip='ip -c'
alias watch='watch -c'

alias pacr='sudo pacman -Rs'
alias pac='sudo pacman -S'
alias pacu='sudo pacman -Syu'
alias pacuu='sudo pacman -Syyuu'
alias pacc='sudo pacman -Rns $(pacman -Qqtd)'
alias pacs='sudo pacman -Ss'
alias pacclean='sudo pacman -Sc'
alias paci='sudo pacman -Qi'

alias fullclean='git checkout master && make clean && rm -f config.h && git reset --hard origin/master'

alias aur='yay -S'
alias auru='yay -Syu'
alias aurs='yay -Ss'
alias top='top -u drew && clear'
alias s='sudo systemctl'
alias S='systemctl --user'

alias sto='stow -t / -d /home/drew/.config/dotfiles/stow/'
alias dosto='cd /home/drew/.config/dotfiles/stow/ && for d in * ; do sto $d ; done && cd -'

alias cpu='watch grep "MHz" /proc/cpuinfo'

alias neofetch='echo "" && neofetch'

alias rank='sudo reflector --verbose -l 200 -p http --sort rate --save /etc/pacman.d/mirrorlist'

alias weather='curl wttr.in/rocester'

alias mp='cat /dev/urandom | tr -dc "[:print:]" | tr -d "[:space:]" | fold -w 25 | grep -E "[A-Z].[0-9].[^a-zA-Z\d]" | head -n 5'

alias hogs='(du -h --exclude="*/mnt/big/*" | sort -shr | head -n 10) 2> /dev/null'

alias wake='openssl s_client -crlf -quiet -connect "friendo.monster:1965" <<<"gemini://friendo.monster/up.gmi" 2>/dev/null | sed -n "4p"'

alias cat='bat -p --wrap character'
alias kat='/usr/bin/cat'
alias cop='xclip -selection clipboard <'
alias ideas='vim /home/drew/Documents/vids/youtube/ideas.md'
alias ds='cat /sys/class/power_supply/sony_controller_battery_a0:ab:51:a5:83:3b/capacity'
alias rmtrash=' rm -rf ~/.mail/posteo/Trash/cur/*'

cm ()
{
    mkdir -p -- "$1" &&
    cd -P -- "$1"
}

# global aliases
alias -g sconf='~/.config/sxhkd/sxhkdrc' bconf='~/.config/bspwm/bspwmrc' bauto='~/bin/bspwm_auto' iconf='~/.config/i3/config' qconf='~/.config/qutebrowser/config.py' conf='~/.config/' dots='~/.config/dotfiles/' qtconf='~/.config/qtile/config.py' suck='~/Documents/suckless/' dwmconf='~/Documents/suckless/dwm' stconf='~/Documents/suckless/st'

# Extract archive
function extract {
    if [ -z "$1" ]; then
        echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    else
        if [ -f $1 ] ; then
            case $1 in
                *.tar.bz2)   tar xvjf ./$1    ;;
                *.tar.gz)    tar xvzf ./$1    ;;
                *.tar.xz)    tar xvJf ./$1    ;;
                *.lzma)      unlzma ./$1      ;;
                *.bz2)       bunzip2 ./$1     ;;
                *.rar)       unrar x -ad ./$1 ;;
                *.gz)        gunzip ./$1      ;;
                *.tar)       tar xvf ./$1     ;;
                *.tbz2)      tar xvjf ./$1    ;;
                *.tgz)       tar xvzf ./$1    ;;
                *.zip)       unzip ./$1       ;;
                *.Z)         uncompress ./$1  ;;
                *.7z)        7z x ./$1        ;;
                *.xz)        unxz ./$1        ;;
                *.exe)       cabextract ./$1  ;;
                *)           echo "extract: '$1' - unknown archive method" ;;
            esac
        else
            echo "$1 - file does not exist"
        fi
    fi
}
