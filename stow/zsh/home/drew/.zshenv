export EDITOR=nvim
export VISUAL=nnnedit
export TERMCMD=st

## nnn
export NNN_PLUG='t:_|st*;m:_|mpv $nnn*;a:_|nnn_mpv_sel*;g:_|gimp $nnn*'
export NNN_USE_EDITOR=1
export NNN_OPENER=nnnopen
export NNN_COLORS='1267'
export NNN_OPTS="R"

## game ting
export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
export __GL_SHADER_DISK_CACHE=1
export PROTON_FORCE_LARGE_ADDRESS_AWARE=1
